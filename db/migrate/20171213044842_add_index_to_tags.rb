class AddIndexToTags < ActiveRecord::Migration[5.1]
  def change
    add_index :taggings, :tag_id unless index_exists?(:taggings, :tag_id)
  end
end
